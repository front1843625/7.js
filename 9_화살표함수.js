/* 화살표 함수 */

// 일반함수의 형태
function add1(a,b){
    return a + b;
}

// function키워드, {}중괄호, return키워드 생략 가능
const add2 = (a,b) => a + b;

console.log(add2(1,2));