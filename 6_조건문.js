/* 조건문 */

const isShow = true; // 참을 의미하는 값

if (isShow){
    console.log('Show!');
}

if(isShow){
    console.log('Show!');
}else{
    console.log('Hide?');
}

/* 반복문 */
for(let i = 1; i<11; i++){
    console.log(i);
}