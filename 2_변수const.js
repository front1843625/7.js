/*변수*/

// const 키워드로 상수 선언
const a = 12;
console.log(a);
//a = 999; 에러 상수는 값 변경 x


// let 키워드로 변수 선언
let b = 12;
console.log(b);
b = 999;
console.log(b);